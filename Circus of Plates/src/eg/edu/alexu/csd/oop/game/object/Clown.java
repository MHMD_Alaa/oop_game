package eg.edu.alexu.csd.oop.game.object;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Clown extends AbstractGameObject {
	public Clown(int x, int y) {
		super(100,200);
		setX(x);
		super.setY(y);
		setVisible(true);
	}
	@Override
	public void setY(int y) {
	}
	@Override
	public void setX(int x) {
		if (x >=70 && x<= 650-150 ) {
			super.setX(x);
		}
	}
	@Override
	public BufferedImage[] getSpriteImages() {
		// TODO Auto-generated method stub
		BufferedImage img = null;
		BufferedImage[] clown = new BufferedImage[1];
		try {
		    img = ImageIO.read(new File("Images//clown.png"));
		    clown[0] = img;
		    
		} catch (IOException e) {
			throw new RuntimeException("path not found");
		}
		return clown;
	}

}
