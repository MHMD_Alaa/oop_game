package eg.edu.alexu.csd.oop.game.object;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Plate extends AbstractGameObject {
	
	private static int width = 50;
	private static int height = 10;
	
private boolean isRight;
	
	public void setPosition(boolean isRight) {
		this.isRight = isRight;
	}
	public boolean isRight() {
		return isRight;
	}
	@Override
	public void setX(int x) {
		if (isRight && x >= 175) {
			super.setX(x);
		}
		if(!isRight && x <= 440) {
			super.setX(x);
		}
		
	}
	public Plate(int x, int y) {
		super(width, height);
		setX(x);
		setY(y);
		setVisible(true);
	}
	
	@Override
	public BufferedImage[] getSpriteImages() {
		// TODO Auto-generated method stub
		BufferedImage img = null;
		BufferedImage[] plate = new BufferedImage[1];
		try {
		    img = ImageIO.read(new File("Images//redPlate.png"));
		    plate[0] = img;
		} catch (IOException e) {
			throw new RuntimeException("path not found");
		}
		return plate;
	}

	

}
