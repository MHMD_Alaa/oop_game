package eg.edu.alexu.csd.oop.game.object;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class leftStick extends AbstractGameObject {
	public leftStick(int x, int y) {
		super(75,100);
		setX(x);
		super.setY(y);
		setVisible(true);
	}
	public void setX(int x) {
		if (x <= 650- 215) {
			super.setX(x);
		}
		
	}
	@Override
	public void setY(int y) {
		// TODO Auto-generated method stub
	}
	@Override
	public BufferedImage[] getSpriteImages() {
		// TODO Auto-generated method stub
		BufferedImage img = null;
		BufferedImage[] rightstick = new BufferedImage[1];
		try {
		    img = ImageIO.read(new File("Images//leftstick.png"));
		    rightstick[0] = img;
		    
		} catch (IOException e) {
			throw new RuntimeException("path not found");
		}
		return rightstick;
	}

}
