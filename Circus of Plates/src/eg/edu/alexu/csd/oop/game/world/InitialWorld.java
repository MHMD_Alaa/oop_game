package eg.edu.alexu.csd.oop.game.world;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.World;
import eg.edu.alexu.csd.oop.game.object.AbstractGameObject;
import eg.edu.alexu.csd.oop.game.object.Clown;
import eg.edu.alexu.csd.oop.game.object.Plate;
import eg.edu.alexu.csd.oop.game.object.leftStick;
import eg.edu.alexu.csd.oop.game.object.rightStick;

public class InitialWorld implements World {
	private List<GameObject> constantObjects;
	private List<GameObject> movableObjects;
	private List<GameObject> controlableObjects;
	private int width;
	private int height;
	private int speed;
	private int rightMaxY;
	private int leftMaxY;

	public InitialWorld(int width, int height, int speed) {
		this.width = width;
		this.height = height;
		this.speed = speed;
		constantObjects = new ArrayList<>();
		constantObjects.add(new AbstractGameObject(800, 700) {

			@Override
			public BufferedImage[] getSpriteImages() {
				setVisible(true);
				BufferedImage img = null;
				BufferedImage[] stage = new BufferedImage[1];
				try {
					img = ImageIO.read(new File("Images//stage.jpg"));
					stage[0] = new BufferedImage(800, 700, BufferedImage.TYPE_INT_ARGB);
					AffineTransform at = new AffineTransform();
					at.scale(0.7, 0.6);
					AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
					stage[0] = scaleOp.filter(img, stage[0]);
				} catch (IOException e) {
					throw new RuntimeException("path not found");
				}
				return stage;
			}

		
		});
		movableObjects = new ArrayList<>();
		controlableObjects = new ArrayList<>();
		controlableObjects.add(new Clown(300, (int) 600 - 220));
		controlableObjects.add(new rightStick(385, 600 - 260));
		rightMaxY = 600 - 260;
		controlableObjects.add(new leftStick(235, 600 - 260));
		leftMaxY = 600 - 260;
	}

	@Override
	public List<GameObject> getConstantObjects() {
		return constantObjects;
	}

	@Override
	public List<GameObject> getMovableObjects() {
		// TODO Auto-generated method stub
		return movableObjects;
	}

	@Override
	public List<GameObject> getControlableObjects() {
		// TODO Auto-generated method stub
		return controlableObjects;
	}

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return width;
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return height;
	}

	@Override
	public boolean refresh() {
		movableObjects.add(new Plate((int) (Math.random() * 650), 10));
		int centerOfPlate = 0;
		int centerOfLeftStick = controlableObjects.get(2).getX() + 25;
		int centerOfRightStick = controlableObjects.get(1).getX() + 50;

		for (int i = 0; i < movableObjects.size(); i++) {
			GameObject temp = movableObjects.get(i);
				temp.setY(temp.getY() + 10);
				centerOfPlate = temp.getX() + 25;
			if (temp.getY() >= 600 ) {
				movableObjects.remove(i);
				continue;
			}
			if (centerOfPlate <= centerOfLeftStick + 25 && centerOfPlate >= centerOfLeftStick - 25&& leftMaxY - temp.getY() == 10 ) {
					leftMaxY -= 10;
					movableObjects.remove(i);
					((Plate) temp).setPosition(false);
					controlableObjects.add(temp);
					continue;

			}
			if (centerOfPlate <= centerOfRightStick + 25 && centerOfPlate >= centerOfRightStick - 25&& rightMaxY - temp.getY() == 10 ) {
				rightMaxY -= 10;
				movableObjects.remove(i);
				((Plate) temp).setPosition(true);
				controlableObjects.add(temp);
				continue;
		}
		}
		
		
		return true;
	}

	@Override
	public String getStatus() {
		// TODO Auto-generated method stub
		return "Score = " + 0 + "   |   Time = 0";
	}

	@Override
	public int getSpeed() {
		// TODO Auto-generated method stub
		return 30;
	}

	@Override
	public int getControlSpeed() {
		// TODO Auto-generated method stub
		return 10;
	}

}
